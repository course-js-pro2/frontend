import Vue from "vue";
import Router from "vue-router";
import News from "./views/News.vue";
import Chats from "./views/Chats.vue";
import Store from "./views/Store.vue";
import Regestration from "./views/Regestration.vue";
import Login from "./views/Login.vue";
import Account from "./views/Account.vue";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      component: News
    },
    {
      path: "/chats",
      component: Chats
    },
    {
      path: "/store",
      component: Store
    },
    {
      path: "/login",
      component: Login
    },
    {
      path: "/reg",
      component: Regestration
    },
    {
      path: "/account",
      component: Account
    }
  ]
});
