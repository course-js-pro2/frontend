import axios from 'axios'

axios.interceptors.request.use(function(config){
    config.headers['authorization'] = localStorage.getItem('jwt')
    return config
})

export default {
    urls: {
        news: '/news',
        chats: '/chats',
        store: '/store',
        users: '/users',
        login: '/login',
        uploads: '/uploads'
    },
    axios: axios
}