import Vue from "vue";
import Vuex from "vuex";
import news from './modules/news';
import chats from './modules/chats';
import store from './modules/store';
import users from './modules/users';
import files from './modules/files';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    news,
    chats,
    store,
    users,
    files
  }
});
