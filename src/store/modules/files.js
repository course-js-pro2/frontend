import api from '@/api';

export default {
    state: {},
    actions: {
        addFile({state, commit}, FD){
            return api.axios.put(api.urls.uploads, FD, {headers: {'Content-Type': 'multipart/form-data'}}).then(res => {
                return res.data
            })
        }
    }
}